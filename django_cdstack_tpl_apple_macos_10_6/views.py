from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static
from django_cdstack_tpl_apple_macos_10_5.django_cdstack_tpl_apple_macos_10_5.views import (
    handle as handle_apple_macos_10_5,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = (
        "django_cdstack_tpl_apple_macos_10_6/django_cdstack_tpl_apple_macos_10_6"
    )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_apple_macos_10_5(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
